import { useState, useEffect } from "react";

import Head from "next/head";
import Phone from "../../../components/phone";
import HomePage from "../../../components/homePage";
import MessagePage from "../../../components/messagePage";
import PaymentsPage from "../../../components/paymentsPage";
import HistoryPage from "../../../components/historyPage";
import CardsPage from "../../../components/cardsPage";
import ProfilePage from "../../../components/profilePage";
import Countdown from "../../../components/Countdown";

export default function Home() {
  const tasks = require("../../../components/tasks.json");

  const [testStarted, setTestStarted] = useState(false);

  const [timerStatus, setTimerStatus] = useState(false);
  const [timeLeft, setTimeLeft] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [taskId, setTaskId] = useState();
  const [taskText, setTaskText] = useState("");
  const [taskState, setTaskState] = useState(false);
  const [currentPage, setCurrentPage] = useState(
      <HomePage
          taskId={taskId}
          taskComplete={taskComplete}
          changePage={changePage}
      />
  );
  useEffect(() => {
    console.log('Init');
    
    function initializeTasks() {
      setTaskId(tasks[6]["id"]);
      setTimeLeft(tasks[6]["time_for_task"]);
      setCurrentTime(tasks[6]["time_for_task"]);
    }
    if (currentPage.type.name === "Home" && !testStarted) {
      setTestStarted(true);
      console.log("start");
      initializeTasks();
    }
  }, [currentPage, testStarted]);
  function changePage(page) {
    switch (page) {
      case `home`:
        setCurrentPage(
          <HomePage
            taskId={taskId}
            taskComplete={taskComplete}
            changePage={changePage}
          />
        );
        break;
      case `Massage`:
        setCurrentPage(
          <MessagePage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `payments`:
        setCurrentPage(
          <PaymentsPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `history`:
        setCurrentPage(
          <HistoryPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `cards`:
        setCurrentPage(
          <CardsPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `profile`:
        setCurrentPage(
          <ProfilePage taskId={taskId} taskComplete={taskComplete} />
        );
        break;

      default:
        break;
    }
  }
  function timeOver() {
    setCurrentPage(
      <HomePage
        taskId={taskId}
        taskComplete={taskComplete}
        changePage={changePage}
      />
    );
    setTimerStatus(false);
    console.log("time over");
    if (tasks[taskId + 1]) {
      setTimeLeft(tasks[taskId + 1]["time_for_task"]);
      setCurrentTime(tasks[taskId + 1]["time_for_task"]);
      setTaskId(tasks[taskId + 1]["id"]);
    } else {
      setTaskText(() => "");
    }
    setTaskState(false);
  }
  function taskComplete() {
    console.log("Task Complete");
    setTaskState(true);
    setTimerStatus(true);
    Countdown(timeLeft, setCurrentTime, timeOver);
  }
  useEffect(() => {
    function taskSetup(taskId) {
      const task = tasks[taskId];
      setCurrentPage(
        <HomePage
          taskId={taskId}
          taskComplete={taskComplete}
          changePage={changePage}
        />
      );
      if (task) {
        setTaskText(() => task["task_text"]);
      }
    }
    if (currentPage.type.name === "Home") {
      taskSetup(taskId);
    }
  }, [taskId]);

  return (
    <div className="container">
      <Head>
        <title>UX_RESEARCH</title>

        <link
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet"
        />
        <script defer src="http://localhost:8092/static/pageWatcher.js?version1"/>
      </Head>

      <main>
        <div className="test-container">
          <div>
            {taskState ? (
              <div className="task-check">
                <span>Успешно</span>
                <div>vvv</div>
              </div>
            ) : (
              ""
            )}
            <div className="text-zone">
              <div>
                <p>{taskText}</p>
              </div>
              <div>
                { timerStatus ? (
                <div className="time-zone">{`00:${
                  currentTime > 9 ? currentTime : `0` + currentTime
                }`}
                </div> ) : (
                  ""
                )}
              </div>
            </div>
            <div className="text-time">
              { timerStatus ? (
              <p>До следующего задания:</p>
              ) : (
                ""
              )}
            </div>
          </div>
          <Phone
            page={currentPage}
            changePage={changePage}
            taskId={taskId}
            taskComplete={taskComplete}
          />
        </div>
      </main>

      <style jsx>{`
        .text-zone,
        .time-zone {
          position: absolute;
          border: solid;
          border-radius: 25px;
          padding: 10px;
          background-color: #f7f2f2;
        }
        .text-time {
          position: absolute;
          width: 300px;
          left: 500px;
        }
        .task-check {
          position: absolute;
          top: -220px;
          left: 589px;
          z-index: 2;
          display: inline;
        }
        .task-check > div {
          background-color: green;
          border: solid green;
          border-radius: 25px;
          content: " ";
          display: inline-block;
          color: green;
        }
        .task-check > span {
          margin-right: 5px;
        }
        .test-container {
          width: 100%;
          position: relative;
        }
        .text-zone {
          width: 200px;
          top: -230px;
          left: 500px;
          padding-top: 20px;
        }
        .time-zone {
          left: 0px;
          top: 280px;
          font-size: 36px;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          width: 100%;
          padding: 0;
          justify-content: space-evenly;
          align-items: center;
        }
        .test-container > div {
          width: 33%;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
