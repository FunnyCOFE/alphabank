import { useState, useEffect } from "react";

import Head from "next/head";
import Phone from "../../../components/1/phone";
import HomePage from "../../../components/1/homePage";
import MessagePage from "../../../components/1/messagePage";
import PaymentsPage from "../../../components/1/paymentsPage";
import HistoryPage from "../../../components/1/historyPage";
import CardPage from "../../../components/1/cardPage";
import CreditCardPage from "../../../components/1/creditCardPage";
import ProfilePage from "../../../components/1/profilePage";
import Countdown from "../../../components/Countdown";
import BonusesPage from "../../../components/1/bonusesPage";

export default function Home() {
  const tasks = require("../../../components/1/tasks.json");

  const [testStarted, setTestStarted] = useState(false);

  const [timerStatus, setTimerStatus] = useState(false);
  const [timeLeft, setTimeLeft] = useState(5);
  const [currentTime, setCurrentTime] = useState(5);
  const [taskId, setTaskId] = useState(0);
  const [taskText, setTaskText] = useState("");
  const [taskState, setTaskState] = useState(false);
  const [currentPage, setCurrentPage] = useState(
      <HomePage
          taskId={taskId}
          taskComplete={taskComplete}
          changePage={changePage}
      />
  );
  useEffect(() => {
    console.log('Init');

    function initializeTasks() {
      setTaskId(tasks[0]["id"]);
      setTimeLeft(tasks[0]["time_for_task"]);
      setCurrentTime(tasks[0]["time_for_task"]);
    }
    if (currentPage.type.name === "CardPage" && !testStarted) {
      setTestStarted(true);
      console.log("start");
      initializeTasks();
    }
  }, [currentPage, testStarted]);
  function changePage(page) {
    switch (page) {
      case `home`:
        setCurrentPage(
            <HomePage
                taskId={taskId}
                taskComplete={taskComplete}
                changePage={changePage}
            />
        );
        break;
      case `message`:
        setCurrentPage(
            <MessagePage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `payments`:
        setCurrentPage(
            <PaymentsPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `history`:
        setCurrentPage(
            <HistoryPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `bonuses`:
        setCurrentPage(
            <BonusesPage taskId={taskId} taskComplete={taskComplete} />
        );
        break;
      case `profile`:
        setCurrentPage(
            <ProfilePage taskId={taskId} taskComplete={taskComplete} />
        );
        break;

      default:
        break;
    }
  }
  function timeOver() {
    if (taskId === 2)
    {
      setTaskText(() => "");
      setTimerStatus(false);
      setTaskState(false);
      return;
    }
    setTimerStatus(false);
    console.log("time over");
    if (tasks[taskId + 1]) {
      setTaskId(tasks[taskId + 1]["id"]);
    } else {
      setTaskText(() => "");
    }
    setTaskState(false);
  }
  function taskComplete() {
    console.log("Task Complete");
    if (tasks[taskId + 1]) {
      setCurrentTime(tasks[taskId + 1]["time_for_task"]);
    }
    setTaskState(true);
    setTimerStatus(true);
    Countdown(timeLeft, setCurrentTime, timeOver);
  }
  useEffect(() => {
    function taskSetup(taskId) {
      const task = tasks[taskId];
      switch (taskId) {
        case 0:
          setCurrentPage(
              <CardPage
                  taskId={taskId}
                  taskComplete={taskComplete}
                  changePage={changePage}
              />
          );
          break;
        case 1:
        case 3:
        case 7:
          setCurrentPage(
              <HomePage
                  taskId={taskId}
                  taskComplete={taskComplete}
                  changePage={changePage}
              />
          );
          break;
        case 2:
          setCurrentPage(
              <CreditCardPage
                  taskId={taskId}
                  taskComplete={taskComplete}
                  changePage={changePage}
              />
          );
          break;
        case 4:
        case 5:
        case 6:
        case 8:
          setCurrentPage(
              <ProfilePage
                  taskId={taskId}
                  taskComplete={taskComplete}
                  changePage={changePage}
              />
          );
          break;
      }

      if (task) {
        setTaskText(() => task["task_text"]);
      }
    }
    taskSetup(taskId);
  }, [taskId]);

  return (
      <div className="container">
        <Head>
          <title>UX_RESEARCH</title>

          <link
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet"
          />
        </Head>

        <main>
          <div className="test-container">
            <div>
              {taskState ? (
                  <div className="task-check">
                    <span>Успешно</span>
                    <div>vvv</div>
                  </div>
              ) : (
                  ""
              )}
              <div className="text-zone">
                <div>
                  <p>{taskText}</p>
                </div>
                <div>
                  { timerStatus ? (
                      <div className="time-zone">{`00:${
                          currentTime > 9 ? currentTime : `0` + currentTime
                      }`}
                      </div> ) : (
                      ""
                  )}
                </div>
              </div>
              <div className="text-time">
                { timerStatus ? (
                    <p>До следующего задания:</p>
                ) : (
                    ""
                )}
              </div>
            </div>
            <Phone
                page={currentPage}
                changePage={changePage}
                taskId={taskId}
                taskComplete={taskComplete}
            />
          </div>
        </main>

      <style jsx>{`
        .text-zone,
        .time-zone {
          position: absolute;
          border: solid;
          border-radius: 25px;
          padding: 10px;
          background-color: #f7f2f2;
        }
        .text-time {
          position: absolute;
          width: 300px;
          left: 500px;
        }
        .task-check {
          position: absolute;
          top: -220px;
          left: 589px;
          z-index: 2;
          display: inline;
        }
        .task-check > div {
          background-color: green;
          border: solid green;
          border-radius: 25px;
          content: " ";
          display: inline-block;
          color: green;
        }
        .task-check > span {
          margin-right: 5px;
        }
        .test-container {
          width: 100%;
          position: relative;
        }
        .text-zone {
          width: 200px;
          top: -230px;
          left: 500px;
          padding-top: 20px;
        }
        .time-zone {
          left: 0px;
          top: 280px;
          font-size: 36px;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          width: 100%;
          padding: 0;
          justify-content: space-evenly;
          align-items: center;
        }
        .test-container > div {
          width: 33%;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
