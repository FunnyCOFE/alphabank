import { useState, useEffect } from "react";

import Head from "next/head";
import HomePage from "../../../components/alfabank_ux_marketing/homePage";
import PopUpPage from "../../../components/alfabank_ux_marketing/PopUpPage";
import PopUpBalancePage from "../../../components/alfabank_ux_marketing/PopUpBalancePage";
import PopUpCreditPage from "../../../components/alfabank_ux_marketing/PopUpCreditPage";
import Countdown from "../../../components/Countdown";
import Phone from "../../../components/alfabank_ux_marketing/phone";
import CardPage from "../../../components/alfabank_ux_marketing/CardPage";
import BalancePage from "../../../components/alfabank_ux_marketing/BalancePage";
import TopPopUpPage from "../../../components/alfabank_ux_marketing/TopPopUpPage";


export default function Home() {
  const tasks = require("../../../components/alfabank_ux_marketing/tasks.json");

  const [testStarted, setTestStarted] = useState(false);

  const [timerStatus, setTimerStatus] = useState(false);
  const [timeLeft, setTimeLeft] = useState(5);
  const [currentTime, setCurrentTime] = useState(5);
  const [taskId, setTaskId] = useState(0);
  const [subTaskId, setSubTaskId] = useState(0);
  const [taskText, setTaskText] = useState("");
  const [taskState, setTaskState] = useState(false);
  const [currentPage, setCurrentPage] = useState(
      <HomePage
          taskId={taskId}
          subTaskId={subTaskId}
          taskComplete={taskComplete}
          changePage={changePage}
          changeSubTask={changeSubTask}
      />
  );
  useEffect(() => {
    console.log('Init');

    function initializeTasks() {
      setTaskId(tasks[0]["id"]);
      setTimeLeft(tasks[0]["time_for_task"]);
      setCurrentTime(tasks[0]["time_for_task"]);
    }
    if (currentPage.type.name === "CardPage" && !testStarted) {
      setTestStarted(true);
      console.log("start");
      initializeTasks();
    }
  }, [currentPage, testStarted]);

  function changeSubTask(id) {
    console.log(subTaskId);
    setSubTaskId(id);
    console.log(id);
    console.log(subTaskId);
  }

  function changePage(page) {
    switch (page) {
      case `home`:
        setCurrentPage(
            <HomePage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'popUp':
        setCurrentPage(
            <PopUpPage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'popUpBalance':
        setCurrentPage(
            <PopUpBalancePage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'card':
        setCurrentPage(
            <CardPage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'popUpCredit':
        setCurrentPage(
            <PopUpCreditPage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'balance':
        setCurrentPage(
            <BalancePage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      case 'topPopUp':
        setCurrentPage(
            <TopPopUpPage
                taskId={taskId}
                subTaskId={subTaskId}
                taskComplete={taskComplete}
                changePage={changePage}
                changeSubTask={changeSubTask}
            />
        );
        break;
      default:
        break;
    }
  }
  function timeOver() {
    if (taskId === 4)
    {
      setTaskText(() => "");
      setTimerStatus(false);
      setTaskState(false);
      return;
    }
    setTimerStatus(false);
    console.log("time over");
    if (tasks[taskId + 1]) {
      setTaskId(tasks[taskId + 1]["id"]);
    } else {
      setTaskText(() => "");
    }
    setTaskState(false);
  }
  function taskComplete() {
    console.log("Task Complete");
    if (tasks[taskId + 1]) {
      setCurrentTime(tasks[taskId + 1]["time_for_task"]);
    }
    setTaskState(true);
    setTimerStatus(true);
    Countdown(timeLeft, setCurrentTime, timeOver);
  }
  useEffect(() => {
    function taskSetup(taskId) {
      const task = tasks[taskId];
      switch (taskId) {
        default:
          setCurrentPage(
              <HomePage
                  taskId={taskId}
                  subTaskId={subTaskId}
                  taskComplete={taskComplete}
                  changePage={changePage}
                  changeSubTask={changeSubTask}
              />
          );
          break;
      }

      if (task) {
        setTaskText(() => task["task_text"]);
      }
    }
    taskSetup(taskId);
  }, [taskId]);

  return (
      <div className="container">
        <Head>
          <title>UX_RESEARCH</title>

          <link
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet"
          />
        </Head>

        <main>
          <div className="test-container">
            <div>
              {taskState ? (
                  <div className="task-check">
                    <span>Успешно</span>
                    <div>vvv</div>
                  </div>
              ) : (
                  ""
              )}
              <div className="text-zone">
                <div>
                  <p className="task-block">{taskText}</p>
                </div>
                <div>
                  { timerStatus ? (
                      <div className="time-zone">{`00:${
                          currentTime > 9 ? currentTime : `0` + currentTime
                      }`}
                      </div> ) : (
                      ""
                  )}
                </div>
              </div>
              <div className="text-time">
                { timerStatus ? (
                    <p>До следующего задания:</p>
                ) : (
                    ""
                )}
              </div>
            </div>
            <Phone
                page={currentPage}
                changePage={changePage}
                taskId={taskId}
                taskComplete={taskComplete}
            />
          </div>
        </main>

      <style jsx>{`
        .task-block {
          white-space: pre-line;
        }
        .text-zone,
        .time-zone {
          position: absolute;
          border: solid;
          border-radius: 25px;
          padding: 10px;
          background-color: #f7f2f2;
        }
        .text-time {
          position: absolute;
          width: 300px;
          left: 400px;
          top: 30px;
        }
        .task-check {
          position: absolute;
          top: -220px;
          left: 589px;
          z-index: 2;
          display: inline;
        }
        .task-check > div {
          background-color: green;
          border: solid green;
          border-radius: 25px;
          content: " ";
          display: inline-block;
          color: green;
        }
        .task-check > span {
          margin-right: 5px;
        }
        .test-container {
          width: 100%;
          position: relative;
        }
        .text-zone {
          width: 300px;
          top: -230px;
          left: 400px;
          padding-top: 20px;
        }
        .time-zone {
          text-align: center;
          top: 310px;
          left: -7px;
          font-size: 36px;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          width: 100%;
          padding: 0;
          justify-content: space-evenly;
          align-items: center;
        }
        .test-container > div {
          width: 33%;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
