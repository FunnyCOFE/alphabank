import {useState, useEffect} from "react";

import Head from "next/head";
import Setting from "../../../components/messenger_font/Settings3";
import Dialog from "../../../components/messenger_font/Dialog3";
import Monolog from "../../../components/messenger_font/Monolog3";
import Countdown from "../../../components/Countdown";
import Phone from "../../../components/messenger_font/phone";
import {completedTaskEventEmitter} from "../../../common/commonFunctions";
import StartPage from "../../../components/messenger_font/StartPage";


export default function Home() {
    const tasks = require("../../../components/messenger_font/tasks.json");

    const [testStarted, setTestStarted] = useState(false);

    const [timerStatus, setTimerStatus] = useState(false);
    const [timeLeft, setTimeLeft] = useState(0);
    const [currentTime, setCurrentTime] = useState(0);
    const [taskId, setTaskId] = useState(-1);
    const [subTaskId, setSubTaskId] = useState(0);
    const [taskText, setTaskText] = useState("");
    const [taskState, setTaskState] = useState(false);
    const [currentPage, setCurrentPage] = useState(
        <StartPage
            taskId={taskId}
            subTaskId={subTaskId}
            taskComplete={taskComplete}
            changePage={changePage}
            changeSubTask={changeSubTask}
        />
    );
    useEffect(() => {
        console.log('Init');

        function initializeTasks() {
            setTaskId(-1);
            setTimeLeft(0);
            setCurrentTime(0);
        }

        if (currentPage.type.name === "StartPage" && !testStarted) {
            setTestStarted(true);
            console.log("start");
            initializeTasks();
        }
    }, [currentPage, testStarted]);

    function changeSubTask(id) {
        console.log(subTaskId);
        setSubTaskId(id);
        console.log(id);
        console.log(subTaskId);
    }

    function nextSection(event) {
        console.debug("nextSection taskId =", taskId);
        if (taskId == 7) {
            taskComplete();
            completedTaskEventEmitter(event, "Messenger Font", taskId, 0, 0, true);
        } else if (taskId == 8) {
            taskComplete();
            completedTaskEventEmitter(event, "Messenger Font", taskId, 0, 0, true);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
        } else if (taskId == 9) {
            window.open("PASTE LINK HERE", "_self");
        } else {
            console.log("Now this is disabled");
        }
    }

    function changePage(page) {
        switch (page) {
            case `Setting`:
                setCurrentPage(
                    <Setting
                        taskId={taskId}
                        subTaskId={subTaskId}
                        taskComplete={taskComplete}
                        changePage={changePage}
                        changeSubTask={changeSubTask}
                    />
                );
                break;
            case 'Dialog':
                setCurrentPage(
                    <Dialog
                        taskId={taskId}
                        subTaskId={subTaskId}
                        taskComplete={taskComplete}
                        changePage={changePage}
                        changeSubTask={changeSubTask}
                    />
                );
                break;
            case 'Monolog':
                setCurrentPage(
                    <Monolog
                        taskId={taskId}
                        subTaskId={subTaskId}
                        taskComplete={taskComplete}
                        changePage={changePage}
                        changeSubTask={changeSubTask}
                    />
                );
                break;
            default:
                break;
        }
    }

    function timeOver() {
        if (taskId === 8) {
            setTaskText(() => "Спасибо! Ваш тест отправлен в обработку. Теперь пройдите, пожалуйста, короткую анкету.");
            setTimerStatus(false);
            setTaskState(false);
            setTaskId(9);
            return;
        }
        setTimerStatus(false);
        console.log("time over");
        if (taskId == -3) {
            setTaskId(0);
        } else if (taskId == -2) {
            setTaskId(3);
        } else if (taskId == -1) {
            setTaskId(6);
        } else if (tasks[taskId + 1]) {
            setTaskId(tasks[taskId + 1]["id"]);
        } else {
            setTaskText(() => "");
            return;
        }
        setTimeLeft(5);
        setCurrentTime(5);
        setTaskState(false);
    }

    function taskComplete() {
        console.log("Task Complete");
        if (tasks[taskId + 1]) {
            setCurrentTime(tasks[taskId + 1]["time_for_task"]);
        }
        setTaskState(true);
        setTimerStatus(true);
        Countdown(timeLeft, setCurrentTime, timeOver);
    }

    useEffect(() => {
        function taskSetup(taskId) {
            const task = tasks[taskId];
            switch (taskId) {
                case 6:
                    setCurrentPage(
                        <Setting
                            taskId={taskId}
                            subTaskId={subTaskId}
                            taskComplete={taskComplete}
                            changePage={changePage}
                            changeSubTask={changeSubTask}
                        />
                    );
                    break;
                case 7:
                    setCurrentPage(
                        <Dialog
                            taskId={taskId}
                            subTaskId={subTaskId}
                            taskComplete={taskComplete}
                            changePage={changePage}
                            changeSubTask={changeSubTask}
                        />
                    );
                    break;
                case 8:
                case 9:
                    setCurrentPage(
                        <Monolog
                            taskId={taskId}
                            subTaskId={subTaskId}
                            taskComplete={taskComplete}
                            changePage={changePage}
                            changeSubTask={changeSubTask}
                        />
                    );
                    break;
                default:
                    setCurrentPage(
                        <StartPage
                            taskId={taskId}
                            subTaskId={subTaskId}
                            taskComplete={taskComplete}
                            changePage={changePage}
                            changeSubTask={changeSubTask}
                        />
                    );
                    break;
            }

            if (task) {
                setTaskText(() => task["task_text"]);
            }
        }

        taskSetup(taskId);
    }, [taskId]);

    return (
        <div className="container">
            <Head>
                <title>UX_RESEARCH</title>

                <link
                    href="https://fonts.googleapis.com/icon?family=Material+Icons"
                    rel="stylesheet"
                />
            </Head>

            <main>
                <div className="test-container">
                    <div>
                        {taskState ? (
                            <div className="task-check">
                                <span>Успешно</span>
                                <div>vvv</div>
                            </div>
                        ) : (
                            ""
                        )}
                        { taskId >= 0 ? (<div className="text-zone">
                                <div>
                                    <p className="task-block">{taskText}</p>
                                </div>
                                <div>
                                    {timerStatus ? (
                                        <div className="time-zone">{`00:${
                                            currentTime > 9 ? currentTime : `0` + currentTime
                                        }`}
                                        </div>) : (
                                        ""
                                    )}
                                </div>
                            </div>
                        ) : ""}
                        <div className="text-time">
                            {timerStatus ? (
                                <p>До следующего задания:</p>
                            ) : (
                                taskId == 7 || taskId == 8 || taskId == 9 ?
                                    (
                                        <button
                                            className="next-button task-button"
                                            onClick={nextSection}
                                        >Дальше
                                        </button>
                                    ) : ""
                            )}
                        </div>
                    </div>
                    <Phone
                        page={currentPage}
                        changePage={changePage}
                        taskId={taskId}
                        taskComplete={taskComplete}
                    />
                </div>
            </main>

            <style jsx>{`
        .task-block {
          white-space: pre-line;
        }
        .next-button {
        width: 200px;
        height: 49px;
        color: white;
        background-color: #4472C4;
        font-size: 20px;
        }
        .text-zone,
        .time-zone {
          position: absolute;
          border: solid;
          border-radius: 25px;
          padding: 10px;
          background-color: #f7f2f2;
        }
        .text-time {
          position: absolute;
          width: 300px;
          left: 400px;
          top: 30px;
        }
        .task-check {
          position: absolute;
          top: -220px;
          left: 589px;
          z-index: 2;
          display: inline;
        }
        .task-check > div {
          background-color: green;
          border: solid green;
          border-radius: 25px;
          content: " ";
          display: inline-block;
          color: green;
        }
        .task-check > span {
          margin-right: 5px;
        }
        .test-container {
          width: 100%;
          position: relative;
        }
        .text-zone {
          width: 300px;
          top: -230px;
          left: 400px;
          padding-top: 20px;
        }
        .time-zone {
          text-align: center;
          top: 310px;
          left: -7px;
          font-size: 36px;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          width: 100%;
          padding: 0;
          justify-content: space-evenly;
          align-items: center;
        }
        .test-container > div {
          width: 33%;
        }
      `}</style>

            <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
        </div>
    );
}
