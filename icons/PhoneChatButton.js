import React from "react";

function PhoneChatButton() {
  return (
      <svg width="24" height="24" viewBox="0 0 24 24" enableBackground="new 0 0 511.995 511.995" xmlns="http://www.w3.org/2000/svg"  fill="#8A8A8E">
        <path d="M23.9949 10.3017C23.9983 10.2015 24 10.1009 24 10C24 4.47715 18.8513 0 12.5 0C6.14873 0 1 4.47715 1 10C1 15.0808 5.35747 19.2766 11 19.9157V23C14 23 24 17.5 24 10.5079C24 10.4375 23.9983 10.3687 23.9949 10.3017ZM7 11H8H17H18V13H17H8H7V11ZM8 7H7V9H8H17H18V7H17H8Z"/>
      </svg>
  );
}

export default PhoneChatButton;
