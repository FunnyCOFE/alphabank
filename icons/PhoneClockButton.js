import React from "react";

function Icon() {
  return (
      <svg width="24" height="24" viewBox="0 0 24 24" enableBackground="new 0 0 511.995 511.995" xmlns="http://www.w3.org/2000/svg" fill="#8A8A8E">
        <path d="M24 11.5C24 17.8513 18.8513 23 12.5 23C6.14873 23 1 17.8513 1 11.5C1 5.14873 6.14873 0 12.5 0C18.8513 0 24 5.14873 24 11.5ZM14 4.5V5.5V12V13V14H13H12H8H7V12H8H12V5.5V4.5H14Z"/>
      </svg>
  );
}

export default Icon;