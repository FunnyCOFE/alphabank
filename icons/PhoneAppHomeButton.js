import React from "react";

function PhoneAppHomeButton() {
  return (
      <svg width="24" height="24" viewBox="0 0 24 24" enableBackground="new 0 0 511.995 511.995"
           xmlns="http://www.w3.org/2000/svg">
        <path d="M1.58296 8.78401C1.21529 9.06805 1 9.50643 1 9.97104V21.5001C1 22.3285 1.67157 23.0001 2.5 23.0001H21.5C22.3284 23.0001 23 22.3285 23 21.5001V9.97104C23 9.50643 22.7847 9.06805 22.417 8.78401L12.6114 1.20865C12.2513 0.930451 11.7487 0.930451 11.3886 1.20865L1.58296 8.78401ZM14 12.2539H10V19.9978H14V12.2539Z"/>
      </svg>
  );
}

export default PhoneAppHomeButton;
