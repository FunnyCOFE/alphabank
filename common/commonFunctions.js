// handleClickOnCardButton props.taskId = 0
// handleExchangeButton props.taskId = 1
// handleCreditCardButton props.taskId = 2
// handleAtmButton props.taskId = 3
// handleClickOnCardButton props.taskId = 4
// handleClickOnCardButton props.taskId = 6
// handleClickOnProfileButton props.taskId = 8

var g_wayId = 0;

const TARGET_CLICK_RESEARCH_EVENT = "__TARGET_CLICK_RESEARCH_EVENT__"
export function completedTaskEventEmitter(parentClickEvent, landingName, taskId, wayId, clickId, completed) {
  if (wayId !== -1) g_wayId = wayId;
  // debugger
  const targetClickEvent = new CustomEvent(TARGET_CLICK_RESEARCH_EVENT, {
    detail: {
      clientX: parentClickEvent.clientX,
      clientY: parentClickEvent.clientY,
      landingName: landingName,
      taskId: taskId,
      wayId: g_wayId,
      clickId: (taskId * 100) + (g_wayId * 10) + clickId,
      completed: completed
    }
  })
  console.log(targetClickEvent)
  console.log((taskId * 100) + (g_wayId * 10) + clickId)

  document.dispatchEvent(targetClickEvent)
}