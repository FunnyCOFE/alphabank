FROM node:12.13.0
ENV LANG=C.UTF-8
COPY . /src/
WORKDIR /src
RUN npm install
RUN npm run build
EXPOSE 3000

CMD ["npm", "run", "dev"]


