import PageImg from "../images/main-page.png";
import { completedTaskEventEmitter } from "../common/commonFunctions";


function Home(props) {
  function handleClickOnProfileButton(event) {
    console.debug("handleClickOnProfileButton props.taskId =", props.taskId);
    if (props.taskId === 8) {
      completedTaskEventEmitter(event, props.taskId);
      const message = JSON.stringify({
        type: '__ALL_TASKS_COMPLETED__',
        value: props.taskId
      });
      setTimeout(() => {
        window.SITE_RESEARCH_COMPLETED = true;
        console.log('research completed');
        window.parent.postMessage(message, '*');
      }, 1000);
      props.changePage("profile")
    } else if (props.taskId === -1) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === 8
    //   ? () => props.changePage("profile")
    //   : props.taskId === -1
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleClickOnCardButton(event) {
    console.debug("handleClickOnCardButton props.taskId =", props.taskId);
    if (props.taskId === 4) {
      completedTaskEventEmitter(event, props.taskId);
      props.changePage("cards");
    } else if (props.taskId === 0) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === 4
    //   ? () => props.changePage("cards")
    //   : props.taskId === 0
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleExchangeButton(event) {
    console.debug("handleExchangeButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 1) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 1
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleBonusButton(event) {
    console.debug("handleBonusButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 8) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
        const message = JSON.stringify({
            type: '__ALL_TASKS_COMPLETED__',
            value: props.taskId
        });
        setTimeout(() => {
            window.SITE_RESEARCH_COMPLETED = true;
            console.log('research completed');
            window.parent.postMessage(message, '*');
        }, 1000);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 8
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleCreditCardButton(event) {
    console.debug("handleCreditCardButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 2) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
        const message = JSON.stringify({
            type: '__ALL_TASKS_COMPLETED__',
            value: props.taskId
        });
        setTimeout(() => {
            window.SITE_RESEARCH_COMPLETED = true;
            console.log('research completed');
            window.parent.postMessage(message, '*');
        }, 1000);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 2
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleAtmButton(event) {
    console.debug("handleAtmButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 3) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 3
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handlePhonePaymentsButton(event) {
    console.debug("handleAtmButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 6) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 6
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }
  function handleTemplatePaymentsButton(event) {
    console.debug("handleAtmButton props.taskId =", props.taskId);
    if (props.taskId === -1) {
      props.changePage("");
    } else if (props.taskId === 6) {
      props.taskComplete();
      completedTaskEventEmitter(event, props.taskId);
    } else {
      console.log("Now this is disabled");
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 6
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }


  return (
    <div className="home-page">
      <style jsx>{`
        .task-button {
          background-color: #0000;
          height: 44px;
          width: 298px;
          top: 134px;
          left: 22px;
          position: absolute;
        }
        .card-button {
          top: 134px;
        }
        .exchange-button {
          top: 922px;
        }
        .credit-card-button {
          top: 210px;
        }
        .atm-button.task-button {
          height: 97px;
          width: 180px;
          top: 1595px;
          left: 10px;
        }
        .bonus-button {
          height: 150px;
          width: 320px;
          top: 660px;
          left: 10px;
        }
        .phone-payments-button.task-button {
          height: 150px;
          width: 320px;
          top: 480px;
          left: 10px;
        }
        
        .template-payments-button.task-button {
          height: 180px;
          width: 320px;
          top: 1050px;
          left: 10px;
        }
        
        .profile-button.task-button {
          width: 224px;
          top: 42px;
        }
      `}</style>
      <div
        className="profile-button task-button"
        onClick={handleClickOnProfileButton}
      >
        {" "}
      </div>
      <div
        className="card-button task-button"
        onClick={handleClickOnCardButton}
      >
        {" "}
      </div>
      <div
        className="exchange-button task-button"
        onClick={handleExchangeButton}
      >
        {" "}
      </div>
      <div
        className="bonus-button task-button"
        onClick={handleBonusButton}
      >
        {" "}
      </div>
      <div
        className="credit-card-button task-button"
        onClick={handleCreditCardButton}
      >
        {" "}
      </div>
      <div
        className="atm-button task-button"
        onClick={handleAtmButton}
      >
        {" "}
      </div>
      <div
        className="phone-payments-button task-button"
        onClick={handlePhonePaymentsButton}
      >
        {" "}
      </div>
      <div
        className="template-payments-button task-button"
        onClick={handleTemplatePaymentsButton}
      >
        {" "}
      </div>
      <img src={PageImg}/>
    </div>
  );
}

export default Home;

