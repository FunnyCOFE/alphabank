import PageImg from "../images/payments-page.png";
import {completedTaskEventEmitter} from "../common/commonFunctions";
// import {useEffect} from React;

function PaymentsPage(props) {
  props.taskComplete();

  return (
      <img src={PageImg} />
  );
}

export default PaymentsPage;
