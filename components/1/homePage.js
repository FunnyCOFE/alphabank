import PageImg from "./images/main-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Home(props) {
    function handleExchangeButton(event) {
        console.debug("handleExchangeButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 1) {
            props.taskComplete();
            completedTaskEventEmitter(event, props.taskId);
        } else {
            console.log("Now this is disabled");
        }
        // props.taskId === -1
        //   ? () => props.changePage("")
        //   : props.taskId === 1
        //   ? () => props.taskComplete()
        //   : () => console.log("Now this is disabled")
    }
    function handleAtmButton(event) {
        console.debug("handleAtmButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 3) {
            props.taskComplete();
            completedTaskEventEmitter(event, props.taskId);
        } else {
            console.log("Now this is disabled");
        }
        // props.taskId === -1
        //   ? () => props.changePage("")
        //   : props.taskId === 3
        //   ? () => props.taskComplete()
        //   : () => console.log("Now this is disabled")
    }
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .exchange-button {
          top: 730px;
          height: 35px;
          width: 320px;
        }
        .atm-button {
          top: 810px;
          height: 35px;
          width: 320px;
        }
       
      `}</style>
            <div
                className="exchange-button task-button"
                onClick={handleExchangeButton}
            >
                {" "}
            </div>
            <div
                className="atm-button task-button"
                onClick={handleAtmButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Home;
