import PageImg from "./images/messages-page.png";

function MessagePage(props) {
    props.taskComplete();
    return (
      <img src={PageImg} />
  );
}

export default MessagePage;
