import PageImg from "./images/credit-card-page.png";
import {completedTaskEventEmitter} from "../../common/commonFunctions";

function CreditCardPage(props) {
    function handleCreditCard(event) {
        console.debug("handleCreditCard props.taskId =", props.taskId)
        if (props.taskId === 2) {
            props.taskComplete()
            completedTaskEventEmitter(event, props.taskId);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: props.taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
        } else {
            console.log("Now this is disabled")
        }
        // props.taskId === -1
        //     ? () => props.changePage("")
        //     : props.taskId === 2
        //     ? () => props.taskComplete()
        //     : () => console.log("Now this is disabled")
    }

    return (
    <div className="cards-page">
      <style jsx>{`
        .task-button {
          height: 240px;
          width: 260px;
          top: 130px;
          left: 40px;
          position: absolute;
        }
      `}</style>
      <div
        className="card-button task-button"
        onClick={handleCreditCard}
      >
        {" "}
      </div>
      <img src={PageImg} />
    </div>
  );
}

export default CreditCardPage;
