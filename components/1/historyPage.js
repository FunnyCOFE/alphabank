import PageImg from "./images/history-page.png";

function HistoryPage(props) {
    props.taskComplete();
    return (
      <img src={PageImg} />
  );
}

export default HistoryPage;
