import PageImg from "./images/card-page.png";
import {completedTaskEventEmitter} from "../../common/commonFunctions";

function CardPage(props) {
    function handleCard(event) {
        console.debug("handleCardButton props.taskId =", props.taskId)
        if (props.taskId === 0) {
            completedTaskEventEmitter(event, props.taskId);
            props.taskComplete();
        } else {
            console.log("Now this is disabled")
        }
        // props.taskId === -1
        //     ? () => props.changePage("")
        //     : props.taskId === 0
        //     ? () => props.taskComplete()
        //     : () => console.log("Now this is disabled")
    }

    return (
    <div className="cards-page">
      <style jsx>{`
        .task-button {
          height: 240px;
          width: 260px;
          top: 110px;
          left: 40px;
          position: absolute;
        }
      `}</style>
      <div
        className="card-button task-button"
        onClick={handleCard}
      >
        {" "}
      </div>
      <img src={PageImg} />
    </div>
  );
}

export default CardPage;
