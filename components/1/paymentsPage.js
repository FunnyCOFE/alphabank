import PageImg from "./images/payments-page.png";

function PaymentsPage(props) {
    props.taskComplete();
    return (
      <img src={PageImg} />
  );
}

export default PaymentsPage;
