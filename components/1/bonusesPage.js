import PageImg from "./images/bonuses-page.png";

function BonusesPage(props) {
    props.taskComplete();
    const message = JSON.stringify({
        type: '__ALL_TASKS_COMPLETED__',
        value: props.taskId
    });
    setTimeout(() => {
        window.SITE_RESEARCH_COMPLETED = true;
        console.log('research completed');
        window.parent.postMessage(message, '*');
    }, 1000);
    return (
      <img src={PageImg} />
  );
}

export default BonusesPage;
