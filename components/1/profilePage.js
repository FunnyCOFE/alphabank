import PageImg from "./images/profile-page.png";
import {completedTaskEventEmitter} from "../../common/commonFunctions";

function ProfilePage(props) {

  function handleBonuses(event) {
    console.debug("handleBonusesButton props.taskId =", props.taskId)
    if (props.taskId === 8) {
      completedTaskEventEmitter(event, props.taskId);
      props.changePage("bonuses");

    } else {
      console.log("Now this is disabled")
    }
    // props.taskId === 8
    //   ? () => props.changePage("bonuses")
    //   : () => console.log("Now this is disabled")
  }

  return (
    <div className="profile-page">
      <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .bonuses-button {
          top: 200px;
          height: 110px;
          width: 320px;
        }
      `}</style>
      <div
        className="bonuses-button task-button"
        onClick={handleBonuses}
      >
        {" "}
      </div>
      <img src={PageImg}/>
    </div>
  );
}

export default ProfilePage;
