import PageImg from "./images/main-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Home(props) {
    function handlePopUpButton(event) {
        console.debug("handlePopUpButton props.taskId =", props.taskId);
        if (props.taskId >= 5) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, false);
            props.changePage("popUp");
        } else if (props.taskId === 0) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 2, 0, true);
            props.taskComplete();
            props.changePage("popUpBalance");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 2, 0, false);
            props.changePage("popUpBalance");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleBalanceButton(event) {
        console.debug("handleBalanceButton props.taskId =", props.taskId);
        if (props.taskId === 0) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, true);
            props.taskComplete();
            props.changePage("balance");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, false);
            props.changePage("balance");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleCardButton(event) {
        console.debug("handleBalanceButton props.taskId =", props.taskId);
        if (props.taskId === 0) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 3, 0, true);
            props.taskComplete();
            props.changePage("card");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 3, 0, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleTopPopUpButton(event) {
        console.debug("handleTopPopUpButton props.taskId =", props.taskId);
        if (props.taskId === 0) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 0, true);
            props.taskComplete();
            props.changePage("topPopUp");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 0, false);
            props.changePage("topPopUp");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleSendButton(event) {
        console.debug("handleSendButton props.taskId =", props.taskId);
        if (props.taskId === 2) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleMobileButton(event) {
        console.debug("handleMobileButton props.taskId =", props.taskId);
        if (props.taskId === 4) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, true);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: props.taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleExchangeButton(event) {
        console.debug("handleExchangeButton props.taskId =", props.taskId);
        if (props.taskId === 3) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 0, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .pop-up-button {
          top: 310px;
          height: 40px;
          width: 320px;
        }
        .balance-button {
          top: 105px;
          height: 90px;
          left: 100px;
          width: 185px;
        }
        .card-button {
          top: 105px;
          height: 90px;
          width: 85px;
        }
        .top-pop-up-button {
          top: 105px;
          left: 290px;
          height: 90px;
          width: 50px;
        }
        .send-button {
          top: 610px;
          left: 160px;
          height: 100px;
          width: 80px;
        }
        .mobile-button {
          top: 1260px;
          left: 160px;
          height: 100px;
          width: 80px;
        }
        .exchange-button {
          top: 1040px;
          height: 40px;
          width: 320px;
        }
      `}</style>
            <div
                className="pop-up-button task-button"
                onClick={handlePopUpButton}
            >
                {" "}
            </div>
            <div
                className="balance-button task-button"
                onClick={handleBalanceButton}
            >
                {" "}
            </div>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <div
                className="top-pop-up-button task-button"
                onClick={handleTopPopUpButton}
            >
                {" "}
            </div>
            <div
                className="mobile-button task-button"
                onClick={handleMobileButton}
            >
                {" "}
            </div>
            <div
                className="send-button task-button"
                onClick={handleSendButton}
            >
                {" "}
            </div>
            <div
                className="exchange-button task-button"
                onClick={handleExchangeButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Home;
