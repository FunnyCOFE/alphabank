import PageImg from "./images/top-pop-up.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function TopPopUp(props) {
    function handleTopUpButton(event) {
        console.debug("handleTopUpButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 1) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 5, 2,true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 0) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 1, true);
            props.changePage("card");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 1, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .top-up-button {
          top: 405px;
          height: 85px;
          left: 63px;
          width: 75px;
        }
        .card-button {
          top: 180px;
          height: 60px;
          width: 320px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <div
                className="top-up-button task-button"
                onClick={handleTopUpButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default TopPopUp;
