import PageImg from "./images/pop-up-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function PopUp(props) {
    function handleBalanceButton(event) {
        console.debug("handleBalanceButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 5) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, true);
            props.changePage("balance");
        } else if (props.taskId === 6) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, false);
            props.changePage("balance");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleBalanceArrowButton(event) {
        console.debug("handleBalanceArrowButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 5) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 1, true);
            props.changePage("popUpBalance");
        } else if (props.taskId === 6) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 1, false);
            props.changePage("popUpBalance");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleCreditCardButton(event) {
        console.debug("handleCreditCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 7) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, false);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: props.taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
            props.changePage("popUpCredit");
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleDollarsButton(event) {
        console.debug("handleDollarsButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 8) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleSalaryButton(event) {
        console.debug("handleSalaryButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 9) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleCreditButton(event) {
        console.debug("handleCreditButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 10) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, true);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: props.taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .balance-button {
          top: 280px;
          height: 70px;
          width: 240px;
        }
        .balance-arrow-button {
          top: 280px;
          height: 70px;
          left: 250px;
          width: 85px;
        }
        .credit-card-button {
          top: 375px;
          height: 70px;
          width: 320px;
        }
        .dollars-button {
          top: 720px;
          height: 60px;
          width: 320px;
        }
        .salary-button {
          top: 475px;
          height: 60px;
          width: 320px;
        }
        .credit-button {
          top: 855px;
          height: 95px;
          width: 320px;
        }
       
      `}</style>
            <div
                className="balance-button task-button"
                onClick={handleBalanceButton}
            >
                {" "}
            </div>
            <div
                className="balance-arrow-button task-button"
                onClick={handleBalanceArrowButton}
            >
                {" "}
            </div>
            <div
                className="credit-card-button task-button"
                onClick={handleCreditCardButton}
            >
                {" "}
            </div>
            <div
                className="dollars-button task-button"
                onClick={handleDollarsButton}
            >
                {" "}
            </div>
            <div
                className="salary-button task-button"
                onClick={handleSalaryButton}
            >
                {" "}
            </div>
            <div
                className="credit-button task-button"
                onClick={handleCreditButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default PopUp;
