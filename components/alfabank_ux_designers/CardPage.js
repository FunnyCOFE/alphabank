import PageImg from "./images/card-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Card(props) {
    function handleTopUpButton(event) {
        console.debug("handleTopUpButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 1) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, -1, 2,true);
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .top-up-button {
          top: 305px;
          height: 85px;
          left: 55px;
          width: 75px;
        }       
      `}</style>
            <div
                className="top-up-button task-button"
                onClick={handleTopUpButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Card;
