import PhoneMoneyButton from "../../icons/PhoneMoneyButton.js";
import PhoneAppButton from "../../icons/PhoneAppButton.js";
import PhoneClockButton from "../../icons/PhoneClockButton.js";
import PhoneAppHomeButton from "../../icons/PhoneAppHomeButton.js";
import PhoneChatButton from "../../icons/PhoneChatButton.js";
import ScrollContainer from "react-indiana-drag-scroll";
import { Component } from "react";
import { completedTaskEventEmitter } from "../../common/commonFunctions";


//export default function Phone(this.props) {
class Phone extends Component {

  constructor(props) {
    super(props);
    this.handlePaymentsPageButton = this.handlePaymentsPageButton.bind(this);
  }

  handlePaymentsPageButton(event) {
    console.debug("Phone handlePaymentsPageButton props.taskId =", this.props.taskId)
    if (this.props.taskId === 1 || this.props.taskId === 4) {
      completedTaskEventEmitter(event, "AlphaBank Designers", this.props.taskId, 4, 0, true);
      this.props.taskComplete()
    } else {
      console.log("Now this is disabled")
    }
  }

  render() {
    const currentPageName = this.props.page.type.name;

    const phone = require("../../images/phone.png");
    const phoneHeader = require("../../images/phone-header.png");
    const phoneHomeButton = require("../../images/phone-home-button.png");
    return (
      <div className="phone-container">
        <style jsx>{`
          .phone-screen {
            position: absolute;
            top: 23px;
            left: 26px;
            width: 343px;
            height: 743px;
            overflow: hidden;
            border-radius: 35px;
          }
          .phone-content {
            position: absolute;
            width: 343px;
            height: 743px;
            color: wheat;
          }
          .phone {
            position: absolute;
          }
          .phone-container {
            margin: auto;
            position: absolute;
            width: 396px;
            height: 789px;
            top: -375px;
            left: 750px;
          }
          .phone-header-text {
            height: 28px;
          }
          .phone-header {
            position: absolute;
            width: 100%;
          }
          .phone-header-img {
            text-align: center;
            position: absolute;
            width: 100%;
          }
          .phone-footer {
            width: max-content;
            position: absolute;
            width: 100%;
            top: 650px;
            background-color: #ffffffe0;
            height: 100px;
          }
          .phone-footer-img {
            width: max-content;
            margin: auto;
          }
          .phone-header-text {
            display: flex;
            justify-content: space-between;
            background-color: #ffffffb5;
            padding-bottom: 35px;
          }
          .phone-header-text > div {
            display: inline-block;
            margin: 10px 25px;
          }
          .phone-elements {
            position: absolute;
            width: 100%;
          }
          .svg {
            width: 24px;
            margin: auto;
          }
          .phone-footer-buttons {
            display: flex;
            justify-content: space-evenly;
            margin: 20px auto 10px;
            font-size: 10px;
            text-align: center;
            color: grey;
          }
          .phone-footer-buttons > div {
            width: 20%;
            cursor: default;
          }
          .active-button {
            color: #EF3124;
            fill: #EF3124;
          }
          .active-block {
            position: absolute;
            top: 112px;
            z-index: 2;
            background-color: black;
            left: 7px;
            width: 328px;
            height: 80px;
          }
          .active-block:before {
            content: "";
          }
          .drag-scroll {
            height: 743px;
            position: relative;
          }
        `}</style>
        <div className="phone">
          <img src={phone}></img>
        </div>
        <div className="phone-screen">
          <div className="phone-content">
            <ScrollContainer nativeMobileScroll={true} horizontal={false} className="phone-scroll">
              <div className="drag-scroll">{this.props.page}</div>
            </ScrollContainer>
            <div className="activeBlock"></div>
          </div>
          <div className="phone-elements">
            <div className="phone-header">
              <div className="phone-header-img">
                <img src={phoneHeader}></img>
              </div>
              <div className="phone-header-text">
                <div>10:00</div>
                <div>
                  <i className="material-icons">&#xe63e;</i>
                </div>
              </div>
            </div>
            { currentPageName === "Home" ?
            <div className="phone-footer">
              <div className="phone-footer-buttons">
                <div
                  className={`home-svg-button ${
                      (currentPageName === `Home` || 
                          currentPageName === `CardPage` || 
                          currentPageName === `ProfilePage` || 
                          currentPageName === `CreditCardPage` 
                      ) ? "active-button" : ""
                  }`}
                >
                  <div className="svg">
                    <PhoneAppHomeButton />
                  </div>
                  <span>Главный</span>
                </div>
                <div className={`clock-svg-button ${
                    currentPageName === `HistoryPage` ? "active-button" : ""
                }`}
                >
                  <div className="svg">
                    <PhoneClockButton />
                  </div>
                  <span>История</span>
                </div>
                <div
                    className={`home-svg-button ${
                        currentPageName === `PaymentsPage` ? "active-button" : ""
                    }`}
                    onClick={this.handlePaymentsPageButton}
                >
                  <div className="svg">
                    <PhoneMoneyButton />
                  </div>
                  Платежи
                </div>
                <div className="app-svg-button">
                  <div className="svg">
                    <PhoneAppButton />
                  </div>
                  Витрина
                </div>
                <div
                  className={`home-svg-button ${
                    currentPageName === `MessagePage` ? "active-button" : ""
                  }`}
                >
                  <div className="svg">
                    <PhoneChatButton />
                  </div>
                  Сообщения
                </div>
              </div>
              <div className="phone-footer-img">
                <img src={phoneHomeButton}></img>
              </div>
            </div>
              : (
              ""
              )}
          </div>
        </div>
      </div>
    );
  }
}

export default Phone;
