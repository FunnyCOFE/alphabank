import PageImg from "./images/balance-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Balance(props) {
    function handleTopUpButton(event) {
        console.debug("handleTopUpButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 1) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 6, 2,true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 0 || props.taskId === 6) {
            props.taskComplete();
            if (props.taskId === 0) completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, true);
            else completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, -1, 2, true);
            props.changePage("card");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 0, 1, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .top-up-button {
          top: 265px;
          height: 85px;
          left: 55px;
          width: 75px;
        }
        .card-button {
          top: 190px;
          height: 65px;
          width: 100px;
          left: 30px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <div
                className="top-up-button task-button"
                onClick={handleTopUpButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Balance;
