import PageImg from "./images/pop-up-page_balance.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function PopUpBalance(props) {
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 6 || (props.taskId === 0)) {
            props.taskComplete();
            if (props.taskId === 0) completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 2, 1, true);
            else completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 1, 2, true);
            props.changePage("card");
        } else if (props.taskId === 1) {
            completedTaskEventEmitter(event, "AlphaBank Designers", props.taskId, 2,  1, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .card-button {
          top: 340px;
          height: 60px;
          width: 320px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default PopUpBalance;
