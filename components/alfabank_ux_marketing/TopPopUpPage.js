import PageImg from "./images/top-pop-up.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function TopPopUp(props) {
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 0) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 1, 2, false);
            props.changePage("card");
        } else if (props.taskId === 1) {
            props.changeSubTask(1);
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 1, 2, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .card-button {
          top: 340px;
          height: 90px;
          left: 200px;
          width: 140px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default TopPopUp;
