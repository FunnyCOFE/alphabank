import PageImg from "./images/pop-up-page-credit.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function PopUpCredit(props) {
    function handleCreditCardButton(event) {
        console.debug("handleCreditCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 7) {
            props.taskComplete();
            props.changeSubTask(0);
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 0, 3, true);
            const message = JSON.stringify({
                type: '__ALL_TASKS_COMPLETED__',
                value: props.taskId
            });
            setTimeout(() => {
                window.SITE_RESEARCH_COMPLETED = true;
                console.log('research completed');
                window.parent.postMessage(message, '*');
            }, 1000);
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-credit-card-page">
            <img src={PageImg} />
        </div>
    );
}

export default PopUpCredit;
