import PageImg from "./images/pop-up-page_balance.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function PopUpBalance(props) {
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 6 || (props.taskId === 0)) {
            props.taskComplete();
            if (props.taskId === 0) completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 2, 4, true);
            else completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 0, 4, true);
            props.changePage("card");
        } else if (props.taskId === 1) {
            props.changeSubTask(2);
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 2, 4, true);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .card-button {
          top: 500px;
          height: 110px;
          left: 210px;
          width: 130px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default PopUpBalance;
