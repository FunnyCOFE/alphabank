import PageImg from "./images/balance-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Balance(props) {
    function handleCardButton(event) {
        console.debug("handleCardButton props.taskId =", props.taskId);
        if (props.taskId === -1) {
            props.changePage("");
        } else if (props.taskId === 0) {
            props.taskComplete();
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 0, 6, true);
            props.changePage("card");
        } else if (props.taskId === 1) {
            props.changeSubTask(0);
            completedTaskEventEmitter(event, "AlphaBank Marketing", props.taskId, 0, 6, false);
            props.changePage("card");
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="pop-up-balance-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .card-button {
          top: 300px;
          height: 200px;
          width: 180px;
        }       
      `}</style>
            <div
                className="card-button task-button"
                onClick={handleCardButton}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Balance;
