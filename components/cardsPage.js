import PageImg from "../images/cards-page.png";
import {completedTaskEventEmitter} from "../common/commonFunctions";

function CardsPage(props) {
  props.taskComplete();

  return (
      <img src={PageImg}/>
  );
}

export default CardsPage;
