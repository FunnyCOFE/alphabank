import PageImg from "./images/monolog_1.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Monolog1(props) {
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
       
      `}</style>
            <img src={PageImg} />
        </div>
    );
}

export default Monolog1;
