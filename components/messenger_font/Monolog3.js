import PageImg from "./images/monolog_3.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Monolog3(props) {
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
       
      `}</style>
            <img src={PageImg} />
        </div>
    );
}

export default Monolog3;
