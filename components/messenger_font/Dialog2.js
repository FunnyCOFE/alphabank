import PageImg from "./images/dialog_2.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Dialog2(props) {
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
       
      `}</style>
            <img src={PageImg} />
        </div>
    );
}

export default Dialog2;
