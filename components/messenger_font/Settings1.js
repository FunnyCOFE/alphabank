import PageImg from "./images/settings_1.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Settings1(props) {
    function incorrectSection(event) {
        console.debug("incorrectSection props.taskId =", props.taskId);
        if (props.taskId == 0) {
            props.taskComplete();
            completedTaskEventEmitter(event, "Messenger Font", props.taskId, 0, 0, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    function correctSection(event) {
        console.debug("correctSection props.taskId =", props.taskId);
        if (props.taskId == 0) {
            props.taskComplete();
            completedTaskEventEmitter(event, "Messenger Font", props.taskId, 1, 0, true);
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .top-button {
          top: 30px;
          height: 430px;
          width: 320px;
        }
        .middle-button {
          top: 460px;
          height: 120px;
          width: 320px;
        }
        .bottom-button {
          top: 580px;
          height: 150px;
          width: 320px;
        }
      `}</style>
            <div
                className="top-button task-button"
                onClick={incorrectSection}
            >
                {" "}
            </div>
            <div
                className="middle-button task-button"
                onClick={correctSection}
            >
                {" "}
            </div>
            <div
                className="bottom-button task-button"
                onClick={incorrectSection}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default Settings1;
