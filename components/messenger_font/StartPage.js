import PageImg from "./images/start-page.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function StartPage(props) {
    function nextSection(event) {
        console.debug("nextSection props.taskId =", props.taskId);
        if (props.taskId < 0) {
            props.taskComplete();
        } else {
            console.log("Now this is disabled");
        }
    }
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
        .next-button {
          top: 670px;
          height: 60px;
          left: 70px;
          width: 220px;
        }
      `}</style>
            <div
                className="next-button task-button"
                onClick={nextSection}
            >
                {" "}
            </div>
            <img src={PageImg} />
        </div>
    );
}

export default StartPage;
