import PageImg from "./images/dialog_1.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Dialog1(props) {
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
       
      `}</style>
            <img src={PageImg} />
        </div>
    );
}

export default Dialog1;
