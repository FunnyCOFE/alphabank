import PageImg from "./images/monolog_2.png";
import { completedTaskEventEmitter } from "../../common/commonFunctions";

function Monolog2(props) {
    return (
        <div className="home-page">
            <style jsx>{`
        .task-button {
          left: 10px;
          position: absolute;
        }
       
      `}</style>
            <img src={PageImg} />
        </div>
    );
}

export default Monolog2;
