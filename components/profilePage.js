import PageImg from "../images/profile-page.png";
import {completedTaskEventEmitter} from "../common/commonFunctions";

function ProfilePage(props) {
  props.taskComplete();
  return (
      <img src={PageImg}/>
  );
}

export default ProfilePage;
