function Countdown(timeLeft, setTimeLeft, timeOver) {
  let timer = timeLeft;
  if (timer > 0) {
    setTimeout(() => {
      setTimeLeft(--timer);
      Countdown(timer, setTimeLeft, timeOver);
    }, 1000);
  } else {
    timeOver();
  return;
  }
}
export default Countdown;
