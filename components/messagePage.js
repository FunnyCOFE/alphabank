import PageImg from "../images/messages-page.png";
import {completedTaskEventEmitter} from "../common/commonFunctions";

function MessagePage(props) {

  function handleCardButton (event) {
    console.debug("handleCardButton props.taskId =", props.taskId)
    if (props.taskId === -1) {
      props.changePage("")
    } else if (props.taskId === 7) {
      props.taskComplete()
      completedTaskEventEmitter(event, props.taskId)
    } else {
      console.log("Now this is disabled")
    }
    // props.taskId === -1
    //   ? () => props.changePage("")
    //   : props.taskId === 7
    //   ? () => props.taskComplete()
    //   : () => console.log("Now this is disabled")
  }

  return (
    <div className="message-page">
      <style jsx>{`
        .task-button {
          background-color: #0000;
          height: 65px;
          width: 315px;
          top: 374px;
          left: 14px;
          position: absolute;
        }
      `}</style>
      <div
        className="chat-button task-button"
        onClick={handleCardButton}
      >
        {" "}
      </div>
      <img src={PageImg} />
    </div>
  );
}

export default MessagePage;
